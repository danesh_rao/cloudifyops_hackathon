module.exports = {
    launch: {
        args: ["--no-sandbox"],
        headless: true,
        //      executablePath: process.env.CHROMIUM_PATH
        //      executablePath: '/usr/bin/chromium-browser'
    },


    testTimeout: 20000,
    testEnvironment: "node",
    server: {
        command: "node server.js",
        launchTimeout: 10000,
        debug: true
    }
};