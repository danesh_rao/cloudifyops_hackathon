const puppeteer = require('puppeteer');
const supertest = require('supertest');
const app = require('./app.js');
const request = supertest(app);

beforeAll(done => {
    done()
})
afterAll(done => {
    done()
})

describe('Home - API Endpoint', () => {
    test("should response the GET method", done => {
        request.get("/")
            .then(response => {
                expect(response.statusCode).toBe(200);
                done();
            });
    });
});
describe('Sharks - API Endpoint', () => {
    test("should response the GET method", done => {
        request.get("/sharks")
            .then(response => {
                expect(response.statusCode).toBe(200);
                done();
            });
    });
});
describe('Contact - API Endpoint', () => {
    test("should response the GET method", done => {
        request.get("/contact")
            .then(response => {
                expect(response.statusCode).toBe(200);
                done();
            });
    });
});

describe('integration test', () => {
    beforeAll(async() => {
        await page.goto('http://localhost:8000/', {
            waitUntil: ["networkidle0", "domcontentloaded"]
        });
    });
    test('all events are binded and htmls are properly displayed', async() => {
        let browser = await puppeteer.launch({
            args: ['--no-sandbox']
        });
        //
        await page.waitForSelector('#home');
        await page.click("#sharksLink");
        await page.waitForSelector('#sharkInfo');
        await page.click("#homeLink");
        await page.waitForSelector('#home');
        await page.click("#contactLink");
        await page.waitForSelector('#contactForm');
        browser.close();
    }, 9000000);
});