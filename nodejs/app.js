const express = require('express');
const app = express();
const router = express.Router();

const path = __dirname + '/views/';

router.use(function (req,res,next) {
  console.log('/' + req.method);
  next();
});

router.get('/', function(req,res){
  res.sendFile(path + 'index.html');
});

router.get('/sharks', function(req,res){
  res.sendFile(path + 'sharks.html');
});

router.get('/contact', (req, res) => {
  res.sendFile(path + 'contact.html');
});

router.get('*', (req,res) => {
  res.status(404).sendFile(path + 'error.html');
});

app.use(express.static(path));
app.use('/', router);



module.exports = app;

